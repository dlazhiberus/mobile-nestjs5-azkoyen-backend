# coffee-back

## Description

Coffee Back

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

# Introduction 
This repository will hold all the files for the Neo Touch Programming Interface that have been created with the NestJS framework

# Getting Started
1.	Installation process
    ...
2.	Software dependencies
    ...
3.	Latest releases
    ...
4.	API references
    ...

# Build and Test

# Contribute
